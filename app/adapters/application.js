import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
   //host: "https://bb2a1617.ngrok.io",
   host: 'http://10.23.209.149:9012',
   namespace: 'graph/import',
   
   shouldReloadAll(){return true;},
   
   shouldBackgroundReloadAll() {
     return true;
   },

   // not needed - we only use 'findAll'
   shouldReloadRecord() {
     return true
   },
   // not needed - we only use 'findAll'
   shouldBackgroundReloadRecord() {
     return true;
   }
});