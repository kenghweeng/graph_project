import Ember from 'ember';

export default Ember.Controller.extend({
    postController: Ember.inject.controller('graph'),
    graphhh: Ember.computed.reads('postController')
});
