import Ember from 'ember';

export default Ember.Controller.extend({
   barData: Ember.computed('model', function(){
         return{
             labels: this.get('model').mapBy('id'),
             datasets: [{
                 label: '2G',
                 backgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBackgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('model').mapBy('two')
             },
             {
                 label: '3G',
                 backgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBackgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('model').mapBy('three')

             },
             {
                 label: '4G',
                 backgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBackgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('model').mapBy('four')
             },
             {
                 label: 'NA',
                 backgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBackgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('model').mapBy('nil')
             }]
         } 
        }),
        barOptions: Ember.computed('model', function(){
         let des = this.get('model').mapBy('desc');
         return {  
             animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.font="12px Arial";
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                        ctx.fillStyle = '#444';
                        var y_pos = model.y * 1.2 // 1.13;
                        console.log(dataset.data[i]);
                        console.log("y_pos" + y_pos);
                        console.log("this is my data from dataset: " + dataset.data[i]);
                        console.log("this is the model.x: " + model.x);
                        // Make sure data value does not get overflown and hidden
                        // when the bar's value is too close to max value of scale
                        // Note: The y value is reverse, it counts from top down
                        if ((scale_max - model.y) / scale_max >= 0.81){ // 0.75
                            console.log("HI I Am HERE");
                            y_pos = model.y * 1.3; // 1.2
                          console.log("final" + scale_max)
                        console.log("final2" + model.y)}
                        if ((scale_max - model.y) / scale_max <= 0.25){
                          console.log("I am too small");
                          y_pos = model.y * 1.07; // 1.03
                        } 
                        //ctx.fillText(dataset.data[i], model.x, y_pos);
                        if(dataset.data[i] != 0){
                        ctx.fillText(dataset.data[i], model.x, y_pos);
                    }
                    }
                });               
            }
        },
             title: {
        display: true,
        text: "Reporting Dashboard"
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
            title: (tooltipsItems)=>{
              // console.log('array:' + des);
              // console.log('index: ' + tooltipsItems[0].index);
              let text = des[tooltipsItems[0].index];
              // //let text;
              // tooltipsItems.forEach(function(tooltipsItem){
              //   console.log('index: ' + tooltipsItem.index)
              //  // text = des[tooltipsItem.index];
              // });
              return text;
            }
        }
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel:{
            display: true,
            labelString: "Questions"
          }
        }],
        yAxes: [{
          stacked: true,
           scaleLabel:{
            display: true,
            labelString: "Number of Teams"
          },
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            }
          }
        }]
      }
    }
  })
});
