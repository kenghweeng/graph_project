import Ember from 'ember';

export default Ember.Controller.extend({
    ajax: Ember.inject.service(),
    spinner: Ember.inject.service("spinner"),
    dropdownDisabled: true,
    flag: false,
    varia: 'abc',
    globalSelected: null,
    deliverySelected: null,
    dataa: [],
    barDataUpdated: [],
    barData: Ember.computed('graphh', function(){
            return{
             labels: this.get('graphh').mapBy('id'),
             datasets: [{
                 label: '2G',
                 backgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBackgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('graphh').mapBy('attributes.two')
             },
             {
                 label: '3G',
                 backgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBackgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('graphh').mapBy('attributes.three')

             },
             {
                 label: '4G',
                 backgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBackgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('graphh').mapBy('attributes.four')
             },
             {
                 label: 'NA',
                 backgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBackgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: this.get('graphh').mapBy('attributes.nil')
             }]
         } 
        }),
        barOptions: Ember.computed('graphh', function(){
         let des = this.get('graphh').mapBy('attributes.desc');
         return {  
             animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                //ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.font="12px Arial";
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                        ctx.fillStyle = '#444';
                        var y_pos = model.y * 1.2 // 1.13;
                        // console.log(dataset.data[i]);
                        // console.log("y_pos" + y_pos);
                        // console.log("this is my data from dataset: " + dataset.data[i]);
                        // console.log("this is the model.x: " + model.x);
                        // Make sure data value does not get overflown and hidden
                        // when the bar's value is too close to max value of scale
                        // Note: The y value is reverse, it counts from top down
                        if ((scale_max - model.y) / scale_max >= 0.81){ // 0.75
                            // console.log("HI I Am HERE");
                            y_pos = model.y * 1.3; // 1.2
                        //   console.log("final" + scale_max)
                        // console.log("final2" + model.y)}
                        }
                        if ((scale_max - model.y) / scale_max <= 0.25){
                        //   console.log("I am too small");
                          y_pos = model.y * 1.07; // 1.03
                        } 
                        //ctx.fillText(dataset.data[i], model.x, y_pos);
                        if(dataset.data[i] != 0){
                        ctx.fillText(dataset.data[i], model.x, y_pos);
                    }
                    }
                });               
            }
        },
             title: {
        display: true,
        text: "Reporting Dashboard"
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
            title: (tooltipsItems)=>{
              // console.log('array:' + des);
              // console.log('index: ' + tooltipsItems[0].index);
              let text = des[tooltipsItems[0].index];
              // //let text;
              // tooltipsItems.forEach(function(tooltipsItem){
              //   console.log('index: ' + tooltipsItem.index)
              //  // text = des[tooltipsItem.index];
              // });
              return text;
            }
        }
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
          scaleLabel:{
            display: true,
            labelString: "Questions"
          }
        }],
        yAxes: [{
          stacked: true,
           scaleLabel:{
            display: true,
            labelString: "Number of Teams"
          },
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            }
          }
        }]
      }
    }
  }),
    actions:{
        selectGlobal(selected){
            Ember.set(this, 'globalSelected', selected);
            Ember.set(this, 'deliverySelected', null);
            console.log('globalhead selected is: ', selected);
            Ember.set(this, 'dropdownDisabled', false);
            this.send('sendGlobalRequest');    
        },
        selectDelivery(selected){
            Ember.set(this, 'deliverySelected', selected);
            console.log("delivery selected is ", selected);
            this.send('sendDeliveryRequest');
        },
        // send action that updates the model to mutate the barData directly.
        sendGlobalRequest(){
            let self = this;
            let model = this.get('graphh');
            self.set('flag', true);
            this.get('spinner').show('global-spinner');
            let result = this.get('ajax').request('/graph/stacked/reports', {data:{globalHead: Ember.get(this,'globalSelected'), deliveryHead: Ember.get(this, 'deliverySelected')}}).then(
                function(result){
                    // self.get('graphh').setObjects(result.data);
                    self.get('dataa').setObjects(result.data);
                    self.set('barDataUpdated',
                    {
            labels: self.get('dataa').mapBy('id'),
             datasets: [{
                 label: '2G',
                 backgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBackgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.two')
             },
             {
                 label: '3G',
                 backgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBackgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.three')

             },
             {
                 label: '4G',
                 backgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBackgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.four')
             },
             {
                 label: 'NA',
                 backgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBackgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.nil')
             }]
         });
                    // console.log(self.get('dataa.labels'));
                    //  let attri = this.get('barData.datasets.firstObject.data');
                    //  this.set(attri, this.get('graphh.'));
                    // self.get('graphh').pushObject(result.data);
                    // console.log('after update');
                    self.get('spinner').hide('global-spinner');
                    return result});
    },
    sendDeliveryRequest(){
            let self = this; 
            let model = this.get('graphh');
            self.set('flag', true);
            self.get('spinner').show('delivery-spinner', {timeout:8000});
            let result = this.get('ajax').request('/graph/stacked/reports', {data:{globalHead: Ember.get(this,'globalSelected'), deliveryHead: Ember.get(this, 'deliverySelected')}}).then(
                function(result){
                    //works in chrome and firefox
                    // self.get('graphh').setObjects(result.data);
                    self.get('dataa').setObjects(result.data);
                    self.set('barDataUpdated',
                    {
            labels: self.get('dataa').mapBy('id'),
             datasets: [{
                 label: '2G',
                 backgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBackgroundColor: "rgba(255, 128, 128,0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.two')
             },
             {
                 label: '3G',
                 backgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBackgroundColor: "rgba(255, 206, 86, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.three')

             },
             {
                 label: '4G',
                 backgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBackgroundColor: "rgba(54, 162, 235, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.four')
             },
             {
                 label: 'NA',
                 backgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBackgroundColor: "rgba(51, 255, 153, 0.5)",
                 hoverBorderWidth: 2,
                 hoverBorderColor: 'lightgrey',
                 data: self.get('dataa').mapBy('attributes.nil')
             }]
         });
                    // console.log(self.get('dataa.labels'));
                    //  let attri = this.get('barData.datasets.firstObject.data');
                    //  this.set(attri, this.get('graphh.'));
                    // self.get('graphh').pushObject(result.data);
                    // console.log('after update');
                    self.get('spinner').hide('delivery-spinner');
                    return result});
    }
    }
});
