import Ember from 'ember';

export default Ember.Controller.extend({
  timeout: 240000,

  alertTime: 120000,

  init: function(){
    this.sessionAlert();
    this.sessionTimeOut();
  },
  sessionTimeOut: function(){
    let self = this;
    Em.run.later(function(){
      self.transitionToRoute('timeout');
    }, this.get('timeout'));
  },
  sessionAlert: function(){
     let self = this;
      Em.run.later(function(){
        alert("Session will timeout in 2min");
    }, this.get('alertTime'));
  },
  
  isAllowedToNextPage: true,

  deliveryLeadName: '',
  deliveryLeadSelected: false,

  teamName: '',
  teamNameSelected: false,
  customTeamNameDisabled: true,

  ansArray: [],
  missingQnsArray: [],

  submitIsDisabled: true,

  totalNumQns: 0,

  actions: {

    saveManager(name) {
      this.set('deliveryLeadName', name);
      this.set('deliveryLeadSelected', true)

    },
    saveTeam(name) {
      this.set('teamName', name);
      this.set('teamNameSelected', true)
      this.set('customTeamNameDisabled', true);
    },
    newTeamName() {
      this.set('customTeamNameDisabled', false);
    },

    ansToggled(choice) {
      // * set Array to correct size (removed)
      // let currentAnsArray = this.get('ansArray');
      // if (currentAnsArray.length == 0) {
      //   let questions = this.get('model.questions');
      //   let totalzNumQns =  questions.mapBy('id').length; // 21
      //   let arrayOfCorrectSize = [];
      //   for (var i = 0; i < totalNumQns; i++) {
      //     arrayOfCorrectSize.push('nil');
      //   }
      //   this.set('ansArray', arrayOfCorrectSize);
      // }

      // console.log(choice); > 'Q1 2G' or 'Q21 3G' or 'Q21 N/A'
      // Save Answer
      let currentAnsArray = this.get('ansArray');
      let length = choice.length; // 5, 6, 7
      let currentQn = choice.substring(1, 3);
      let currentQnIndex = parseInt(currentQn, 10) - 1;
      let currentAnsUntrimmed = choice.substring(length - 3, length);
      let currentAns = currentAnsUntrimmed.trim();
      
      currentAnsArray[currentQnIndex] = currentAns;
      this.set('ansArray', currentAnsArray);

      if (currentAnsArray.length == 21 && !currentAnsArray.includes(undefined)) {
        if (this.get('deliveryLeadName') != '' &&  this.get('teamName') != '') {
          this.set('submitIsDisabled', false);
        }
      }
    },

    performValidation() {
      let teamName = this.get('teamName');
      console.log(teamName);
      let teamNameSelected = this.get('teamNameSelected');
      console.log(teamNameSelected);
      let customTeamNameDisabled = this.get('customTeamNameDisabled');
      console.log(customTeamNameDisabled);

   


      // if (this.get('teamNameSelected') && this.get('deliveryLeadSelected')) {
      //   if (!(this.get('ansArray'.includes('nil')))) {
      //     this.set('submitIsDisabled', false);
      //   }
      // }


      // Validation: method has changed
      // length is hard-coded here
      // let lengthAfterCurrentAns = currentAnsArray.length;
      // if (lengthAfterCurrentAns === 21) {
      //   let arrIsFull = true;
      //   for (var i = 0; i < lengthAfterCurrentAns; i++) {
      //     if (typeof(currentAnsArray[i])=='undefined') {
      //       arrIsFull = false;
      //     }
      //   }
      //   if (arrIsFull && teamName!='' && deliveryLeadName !='' ) {
      //     this.set('isDisabled', false);
      //   } else {
      //     this.set('isDisabled', true);
      //   }
      // }

      // let questions = this.get('model.questions');
      // let totalNumQns =  questions.mapBy('id').length; // 21
      // this.set('totalNumQns', totalNumQns);

      // let missingQnsOnSave = [];
      // let ansArrayToSave = this.get('ansArray');
      // // Just validate quetions for now. later add teamname & delivery lead.
      // // let teamNameToSave= this.get('teamName');
      // // let deliveryLeadNameToSave= this.get('deliveryLeadName');
      // for (var i = 0; i < totalNumQns; i++) {
      //   if (typeof(ansArrayToSave[i])=='undefined') {
      //     missingQnsOnSave.push(i);
      //   }
      // }
      // console.log(missingQnsOnSave);
      // this.set('missingQnsArray', missingQnsOnSave);
    },

    // * Moved to ansToggled()
    // validateQuestions() {
      
    // },

    saveResponse() {
     // this.send('confirmSubmit');
      let ansArrayToSave = this.get('ansArray');
      let teamNameToSave= this.get('teamName');
      let deliveryLeadNameToSave= this.get('deliveryLeadName');
      // save record in store
      const newAns = this.store.createRecord('answer', { 
        teamName: teamNameToSave,
        deliveryLeadName: deliveryLeadNameToSave,
        ans: ansArrayToSave
      }); 
      // POSTS to /answers
      newAns.save();
      alert("Thank you! We've just saved your response!");
      this.transitionToRoute('success');
      // this.set('responseMessage', `Thank you! We've just saved your response`);
      // Should I reset ansArray back to an empty array?
      // this.set('ansArray', []); 
    },

    checkMissingQns() {
      console.log(this.get('missingQnsArray'));
    },
    // confirmSubmit: function(){
    //   return confirm('Do you really want to submit the form?');
    // }
   
  },
});