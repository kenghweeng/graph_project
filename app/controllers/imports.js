import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    uploadSuccess(data) {
      // console.log('result is: ' + data);
     // this.transitionToRoute('reports', data);
      //this.send('reportsFunction', data);
      //this.transitionToRoute('reports')
      this.set("successMessage", data);
      window.setTimeout(function(){location.reload()},3000)
    },
     uploadError(error) {
      // console.log('result is: ' + error);
     // this.transitionToRoute('reports', data);
     this.set("errorMessage", error);
      window.setTimeout(function(){location.reload()},3000)
    }
  }
});
