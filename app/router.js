import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('reports');
  this.route('imports');
  this.route('graph');
  this.route('questions');
  this.route('success');
  this.route('timeout');
  this.route('trend');
});

export default Router;
