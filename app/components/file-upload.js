import EmberUploader from 'ember-uploader';

export default EmberUploader.FileField.extend({
  filesDidChange: function(files) {
    const uploader = EmberUploader.Uploader.create({
      url: this.get('url')
    });

    /*
    if (!Ember.isEmpty(files)) {
      // this second argument is optional and can to be sent as extra data with the upload
      uploader.upload(files[0]);
      
    }
    */
    
    uploader.upload(files[0]).then(data => {
      // console.log('file upload success');
      // alert("Success");
      // console.log(data);
      // console.log(data.data[0]);

      //console.log('file upload data received', data);
      //console.log('JSON stringify: ' + JSON.stringify(data));
      this.sendAction('success', data);
    
    }, error => {
      // Unaccounted Case: if App Server is not up, no JSON response received -> no errors hash to display error msg.
      //console.log('upload failure' + error);
      console.log(error);
      let errorMsg = error.responseJSON.errors[0].detail;
      console.log('received obj is', errorMsg);
      this.sendAction('error', errorMsg);
      window.location.reload(true);
    })

  }
});