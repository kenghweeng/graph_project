import Ember from 'ember';

export default Ember.Component.extend({

  // allowedToNextPage: neither tro nor false,
  
  tagName: "section",
  // The page we are currently on

  page: 1,
  // The number of items to show per page

  paginateBy: 10,
  // Returns the list of items for the current page only

  paginatedItems: Ember.computed('items', 'page', function(){
    var i = (parseInt(this.get('page')) - 1) * parseInt(this.get('paginateBy'));
    var j = i + parseInt(this.get('paginateBy'));
    return this.get('items').slice(i, j);
  }),
  // The total number of pages that our items span

 currentPage: Ember.computed('page', function(){
    return (this.get('page'));
  }),

  numberOfPages: Ember.computed('page', function(){
    var n = this.get('items.length');
    var c = parseInt(this.get('paginateBy'));
    var r = Math.floor(n/c);
    if(n % c > 0) {
      r += 1;
    }
    return r;
  }),
  // An array containing the number of each page: [1, 2, 3, 4, 5, ...]

  pageNumbers: Ember.computed('numberOfPages', function(){
    var n = Array(this.get('numberOfPages'));
    for(var i = 0;i < n.length;i++) {
      n[i] = i + 1;
    }
    return n;
  }),
  // Whether or not to show the "next" button

  showNext: Ember.computed('page', function(){
    return (this.get('page') < this.get('numberOfPages'));
  }),
  // Whether or not to show the "previous" button

  showPrevious: Ember.computed('page', function(){
    return (this.get('page') > 1);
  }),
  // The text to display on the "next" button

  nextText: 'Next page',
  // The text to display on the "previous" button

  previousText: 'Previous page',
  currentSize:0,
  currentItems: Ember.computed('page', function(){
    let sum = this.get('currentSize') + this.get('paginatedItems').length;
    return (this.set('currentSize',sum));
  }),
  actions: {
    
    // * method used for debugging
    // checkStatus() {
    //   let currentItems = this.get('currentItems');
    //   console.log("currentItems is: " + currentItems);
    //   let currentArr = this.get('currentArr');
    //   console.log("currentArr is below ");
    //   console.log(currentArr);
    //   let currentPage = this.get('currentPage');
    //   console.log("currentPage is: " + currentPage);
    //   let arrCurrentPage = this.get('arrCurrentPage');
    //   console.log("arrCurrentPage is below: ");
    //   console.log(arrCurrentPage);
    // },
    
    // Show the next page of items
    nextClicked() {
      // * validation now done within this method
      // this.send('checkPageItems');
      
      // * currentItems is buggy (keeps counting even after clicking on previous page)
      // let currentItems = this.get('currentItems');
      // console.log("currentItems is: " + currentItems);

      let currentArr = this.get('currentArr');
      // console.log("currentArr is below ");
      // console.log(currentArr);
      let currentPage = this.get('currentPage');
      // console.log("currentPage is: " + currentPage);
      
    
      if (currentArr.length < currentPage * 5) {
        // console.log('currentArr length is too short');
        this.set('allowedToNextPage', false);
        let qnMissed = "";
        for (var i=0; i < currentPage*5; i++){
          if (typeof(currentArr[i])=='undefined'){
            
            qnMissed= qnMissed+ "Q"+(i+1) + " ";
            let index = i%5;
          // console.log("index" + index);
           // $('#'+index).css("background-color","#ff0000");

          }
          let name="";
          if (this.get('paginationDLName') == '') {
            name = "Delivery Name is missing";
        }
         if (this.get('paginationTeamName') == '') {
            name = " TeamName is missing";
        }
         
        }
         alert("Please answer the follower questions before proceeding: "+ qnMissed  );

      } else {
        // console.log('currentArr length is just nice!');
        let arrCurrentPage = currentArr.slice(0, currentPage * 5);
        this.set('arrCurrentPage', arrCurrentPage);
        if (arrCurrentPage.includes(undefined)) {
          // console.log('Some questions not attempted!');
          let missingQns = [];
            let qnMissed = "";

          for (let i = 0; i < arrCurrentPage.length; i++) {
            if (typeof(arrCurrentPage[i])=='undefined') {
              let qnNum = i + 1;
              missingQns.push('Q' + qnNum);
              qnMissed= qnMissed+ "Q"+(i+1) + " ";
              let index = i%5;
            }
          }
          this.set('currentMissingQnsArr', missingQns);
          this.set('allowedToNextPage', false);
          alert("Please answer the follower questions before proceeding: "+ qnMissed  );
        } else {
          // console.log('All questions attempted');
          this.set('allowedToNextPage', true);
        }
      }

      if (this.get('currentPage') == 1) {
        if (this.get('paginationDLName') == '' || this.get('paginationTeamName') == '') {
          this.set('allowedToNextPage', false);
          this.set('teamInfoEnabled', false); // not being used
        }
      }

      if (this.get('allowedToNextPage')) {
        if(this.get('page') + 1 <= this.get('numberOfPages')) {
        this.set('page', this.get('page') + 1);
      }
       document.getElementById("top").click();

      }
     
    },

      
    // Show the previous page of items

    previousClicked() {
      document.getElementById("top").click();
      this.set('allowedToNextPage', true)

      if(this.get('page') > 0) {
        this.set('page', this.get('page') - 1);
      }
    },
    // Go to the clicked page of items

    pageClicked(pageNumber){
      this.set('page', pageNumber);
    },

    check() {
      console.log(this.get('currentMissingQnsArr'));
    }

  }
});