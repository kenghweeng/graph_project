import Ember from 'ember';

export default Ember.Component.extend({
    change: function(event){
        let value = event.target.value;
        console.log("delivery-lead", value);
        this.attrs.onchange(value);
    }
});