import DS from 'ember-data';

export default DS.Model.extend({
    two: DS.attr(),
    three: DS.attr(),
    four: DS.attr(),
    nil: DS.attr(),
    desc: DS.attr('string')
});