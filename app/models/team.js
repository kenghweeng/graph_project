import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  // don't take other attributes for now
});
