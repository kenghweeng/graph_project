import DS from 'ember-data';

export default DS.Model.extend({
  questionID: DS.attr('string'),
  description: DS.attr('string'),
  category: DS.attr('string'),
  nil: DS.attr('string'),
  two: DS.attr('string'),
  three: DS.attr('string'),
  four: DS.attr('string'),
  // computed property
  // set default answer to NIL
  ans: Ember.computed('questionID', function() {
    return `${this.get('questionID')} NIL`;
  }),
});
