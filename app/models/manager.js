import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  role: DS.attr('string'),
  // manager_name: DS.attr('string') > underscore is causing problem
});
