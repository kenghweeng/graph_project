import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  normalize(typeClass, hash) {
    hash.attributes.two = hash.attributes.options.two;
    hash.attributes.three = hash.attributes.options.three;
    hash.attributes.four = hash.attributes.options.four;
    hash.attributes.nil = hash.attributes.options.nil;

    delete hash.attributes.options;

    return this._super(...arguments);
  },

  attrs: {
    questionID: 'questionID'
  }
});
