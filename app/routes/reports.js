import Ember from 'ember';

export default Ember.Route.extend({
  
  /*actions: {
    reportsFunction(data){
      console.log(data);
    }
  },*/ 
  
  model() {
    //console.log(receivedData);
    this.store.unloadAll('report');
    return this.get('store').findAll('report', {reload: true});
    // GET <namespace>/reports
  },

  // afterModel() {
  //   this.get('store').unloadAll('report');
  // }
  
});
