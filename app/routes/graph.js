import Ember from 'ember';

export default Ember.Route.extend({
    ajax: Ember.inject.service(),

    model(){
        return Ember.RSVP.hash({
            global: this.get('ajax').request('/managers/globalHeads'),
            delivery: this.get('ajax').request('/managers/deliveryHeads'),
            graph: this.get('ajax').request('/graph/stacked/reports', {data:{globalHead:null, deliveryHead: null}})
        });
    },

    setupController(controller, model){
        this._super(...arguments);
        Ember.set(controller, 'globalheads', model.global.data);
        Ember.set(controller, 'deliveryleads', model.delivery.data);
        Ember.set(controller, 'graphh', model.graph.data);
    }
});
