import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  // model() {
  //   return this.store.findAll('question');

  // }
init(){
  jQuery(window).on('beforeunload', function(){
    return "Are you sure you want to leave?";
  })
},
  model() {
    return Ember.RSVP.hash({
      questions: this.store.findAll('question'),
      managers: this.store.findAll('manager'),
      teams: this.store.findAll('team')
    });
    
  },

  // setupController(controller, models) {
  //   controller.set('questions', models.questions);
  //   controller.set('managers', models.managers);
  //   // or, more concisely:
  //   // controller.setProperties(models);
  // }
  

  // model(){}

  // isWide: false, should define attributes here, or in controller?

  // actions: {
  //   ansToggled: function(event) {
  //     console.log("changing answer choice", event);
  //     // this.toggleProperty(isWide);
  //     // console.log('is wide is ' + isWide);
  //   },
  // },


});
